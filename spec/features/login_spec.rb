# frozen_string_literal: true
require 'rails_helper'

RSpec.feature 'Login' do
  background do
    visit '/login'
  end

  scenario 'login page' do
    expect(current_path).to eq('/login')
    expect(page).to have_text('Please sign in')
  end

  scenario 'attempt to login' do
    click_button 'Sign in'
    expect(current_path).to eq('/dashboard')
  end
end
