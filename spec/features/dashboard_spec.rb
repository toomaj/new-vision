# frozen_string_literal: true
require 'rails_helper'

RSpec.feature 'Dashboard' do
  scenario 'index' do
    visit '/dashboard'
    expect(current_path).to eq('/dashboard')
    expect(page).to have_text('Here is Dashboard!')
  end
end
