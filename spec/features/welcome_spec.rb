# frozen_string_literal: true
require 'rails_helper'

RSpec.feature 'Welcome' do
  background do
    visit '/'
  end

  scenario '/index' do
    expect(current_path).to eq('/')
    expect(page).to have_text('New Vision')
  end
end
