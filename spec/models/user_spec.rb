# frozen_string_literal: true
require 'rails_helper'

RSpec.describe User, type: :model do
  subject { build :user }

  it { is_expected.to validate_presence_of(:first_name) }
  it { is_expected.to validate_length_of(:first_name) }

  it { is_expected.to validate_presence_of(:last_name) }
  it { is_expected.to validate_length_of(:last_name) }

  it { is_expected.to validate_presence_of(:email) }
  it { is_expected.to validate_length_of(:email) }

  describe '#email' do
    valid_emails = ['mail@example.com', 'mail@example',
                    '123@432', Faker::Internet.email, Faker::Internet.email]

    valid_emails.each do |mail|
      it { is_expected.to allow_value(mail).for(:email) }
    end

    %w(mail@ @example 123432 ' " % c6 # 2@ - _).each do |mail|
      it { is_expected.not_to allow_value(mail).for(:email) }
    end
  end

  describe '#password' do
    let(:mail) { Faker::Internet.email }
    let(:prefix) { Faker::Lorem.word }
    let(:suffix) { Faker::Lorem.word }
    let(:password_with_email) { "#{prefix}#{mail}#{suffix}" }
    before { subject.email = mail }

    it { is_expected.not_to allow_value(password_with_email).for(:password) }

    weak_passes = ['1234567890', 'UIUERIWEURIWUI', '-*hello*@^%*@^',
                   'OiuYhh1', '10101oo!ag10', 'ABCDEF!@JK123']

    good_passes = ['a good PasWord am I', 'Hello12345', '*7i4-_2L',
                   'HGEuejqwaeH', '****jJk@@@@@', 'can not Find me',
                   '12345678901234567890']

    weak_passes.each do |pass|
      it { is_expected.not_to allow_value(pass).for(:password) }
    end

    good_passes.each do |pass|
      it { is_expected.to allow_value(pass).for(:password) }
    end
  end
end
