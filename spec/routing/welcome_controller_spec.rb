# frozen_string_literal: true
require 'rails_helper'
RSpec.describe WelcomeController, type: :routing do
  describe 'GET /' do
    subject { { get: '/' } }
    it { is_expected.to route_to('welcome#index') }
  end
end
