# frozen_string_literal: true
require 'rails_helper'
RSpec.describe LoginController, type: :routing do
  describe 'GET /new' do
    subject { { get: '/login' } }
    it { is_expected.to route_to('login#new') }
  end

  describe 'POST /login' do
    subject { { post: '/login' } }
    it { is_expected.to route_to('login#create') }
  end
end
