# frozen_string_literal: true
require 'rails_helper'

RSpec.describe ApplicationController, type: :controller do
  let(:user) { create :user }

  controller do
    def index
      @user = user
      head :nothing
    end
  end

  before do
    session[:user_id] = user.try(:id)
    get :index
  end

  it 'assigns user' do
    expect(assigns(:user)).to eq(user)
  end
end
