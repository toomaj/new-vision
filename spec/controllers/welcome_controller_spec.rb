# frozen_string_literal: true
require 'rails_helper'

RSpec.describe WelcomeController, type: :controller do
  subject { response }

  before { run }
  context 'GET index' do
    def run
      get :index
    end

    it { is_expected.to have_http_status :ok }
  end
end
