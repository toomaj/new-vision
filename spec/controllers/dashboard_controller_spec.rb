# frozen_string_literal: true
require 'rails_helper'

RSpec.describe DashboardController, type: :controller do
  subject { response }

  context 'GET index' do
    it { is_expected.to have_http_status :ok }
  end
end
