# frozen_string_literal: true
require 'rails_helper'

RSpec.describe LoginController, type: :controller do
  let!(:user) { create :user }

  subject { response }
  before { run }

  context 'GET new' do
    def run
      get :new
    end

    it { is_expected.to have_http_status :ok }
  end

  context 'Login' do
    def run
      post :create
    end

    context 'successful login' do
      it { is_expected.to have_http_status 302 }
      it { is_expected.to redirect_to '/dashboard' }
    end
  end
end
