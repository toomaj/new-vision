# frozen_string_literal: true
class User < ActiveRecord::Base
  attr_accessor :password

  validates :first_name, presence: true
  validates :first_name, length: { maximum: 500 }

  validates :last_name, presence: true
  validates :last_name, length: { maximum: 500 }

  validates :email, presence: true
  validates :email, length: { maximum: 500 }
  validates :email, format: /\A.+@.+\z/

  validates :password, confirmation: true, length: { minimum: 8 }

  validate :password_does_not_contain_email
  validate :password_format, unless: proc { password.length >= 20 }

  def password_does_not_contain_email
    return unless email
    return unless password.downcase.include?(email.downcase)
    errors.add(:password, 'should not contain email')
  end

  def password_format
    # password regex can change to suite any password strength requirements
    return if password =~ /\A(?=.*[A-Z])(?=.*[a-z]).*\z/
    msg = 'must contain at least one lowercase and one uppercase letter'
    errors.add(:password, msg)
  end

  def password=(entry_pass)
    @password = entry_pass
    self.password_digest = nil
    return if entry_pass.blank?
    self.password_digest = SCrypt::Password.create(password)
  end
end
