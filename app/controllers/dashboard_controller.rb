# frozen_string_literal: true
class DashboardController < ApplicationController
  def index
    @user = user
  end
end
