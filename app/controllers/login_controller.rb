# frozen_string_literal: true
class LoginController < ApplicationController
  def new
  end

  def create
    redirect_to '/dashboard'
  end
end
