# frozen_string_literal: true
class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception

  before_action :user

  def user
    session[:user_id] && User.find(session[:user_id])
  end
end
