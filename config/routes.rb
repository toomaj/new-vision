# frozen_string_literal: true
Rails.application.routes.draw do
  root to: 'welcome#index'

  scope 'dashboard' do
    get '/' => 'dashboard#index'
  end

  scope 'login' do
    get '/' => 'login#new'
    post '/' => 'login#create'
  end
end
